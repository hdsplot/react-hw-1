import React, { Component } from 'react';

import Button from './components/Button';
import Modal from './components/Modal';

import './App.scss';

class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
  }

  showFirstModal = () => {
    this.setState({ firstModal: true });
  }
  showSecondModal = () => {
    this.setState({ secondModal: true });
  }

  closeModal = () => {
    this.setState({ firstModal: false })
    this.setState({ secondModal: false })
  }

  render() {
    return (
      <div className="page-wrapper">
        <Button className="btn" textBtn={"Open first modal"} backgroundColor={"#a12b2b"}
          onClick={this.showFirstModal} />
        <Button className="btn" textBtn={"Open second modal"} backgroundColor={"#2a682a"}
          onClick={this.showSecondModal} />

        {this.state.firstModal ?
          <Modal header={"Do you want delete this file?"}
            text={"Once you delete this file, it want be possible to undo this action. Are you sure you want to delete it?"}
            backgroundColor={"#c63737"} headerBg={"#a12b2b"} closeButton={true} actions={
              <>
                <Button className="btn" textBtn={"Ok"} backgroundColor={"#871919"} onClick={this.closeModal} />
                <Button className="btn" textBtn={"Cancel"} backgroundColor={"#871919"} onClick={this.closeModal} />
              </>
            } onClick={this.closeModal} /> : ''}

        {this.state.secondModal ?
          <Modal header={"Are you sure?"} text={"Press any button "} backgroundColor={"#508350"} headerBg={"#2a682a"} closeButton={true} actions={
            <>
              <Button className="btn" textBtn={"Yes"} backgroundColor={"#1b511b"} onClick={this.closeModal} />
              <Button className="btn" textBtn={"No"} backgroundColor={"#1b511b"} onClick={this.closeModal} />
            </>
          } onClick={this.closeModal} /> : ''}
      </div>
    )
  }
}

export default App;