import React, {Component} from 'react';

import './Button.scss'

class Button extends Component {
  render() {
    const { textBtn, backgroundColor, onClick } = this.props;

    return (
      <button className="btn" onClick={onClick} style={{ backgroundColor: backgroundColor }}>{textBtn}</button>
    );
  }
}

export default Button;
