import React, {Component} from 'react';
import './Modal.scss';

class Modal extends Component {

    render() {
        const { header, text, actions, closeButton, onClick, backgroundColor, headerBg } = this.props;

        return (
            <>
                <div className="modal" style={{ backgroundColor: backgroundColor }}>
                    <header className="modal__header" style={{ backgroundColor: headerBg }}>
                        <h2 className="modal__header-title">{header}</h2>
                        {closeButton ? <button className="modal__header-closeBtn" onClick={onClick}>X</button> : ''}
                    </header>
                    <div className="modal__content">
                        <p className="modal__content-text">{text}</p>
                        <div className="modal__content__buttons">{actions}</div>
                    </div>
                </div>
                <div className="modal-page-bg" onClick={onClick} />
            </>
        );
    }
}

export default Modal;
